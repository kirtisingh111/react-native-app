import React, { Component } from 'react';
import {Alert,Button, Text, Image, View, StyleSheet, ScrollView } from 'react-native';
// import ModalExample from './Component/Modal/modal';



class ScrollViewExample extends Component {
   state = {
      names: [
         {'name': 'Ben Demo', 'id': 1},
         {'name': 'Susan Demo', 'id': 2},
         {'name': 'Robert Demo', 'id': 3},
         {'name': 'Mary Demo', 'id': 4},
         {'name': 'Daniel v', 'id': 5},
         {'name': 'Laura Demo', 'id': 6},
         {'name': 'John Demo', 'id': 7},
         {'name': 'Debra Demo', 'id': 8},
         {'name': 'Aron Demo', 'id': 9},
         {'name': 'Ann Demo', 'id': 10},
         {'name': 'Steve Demo', 'id': 11},
         {'name': 'Olivia Demo', 'id': 12}
      ]
   }

   render() {
       const showAlert = () =>{
      Alert.alert(
         'You need to...'
      )
   }
      return (
         <View>
            <ScrollView style = {styles.view}>
               <Image  style={styles.Image} source = {require('C:/Users/Kirti Singh/Desktop/random-people/src/mygate.png')} />
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text onPress={showAlert}>{item.name}<br/><br/>
                          <div>{item.name} &nbsp; {item.name}</div><br/>
                          <small>Allowed By You </small>
                          <hr/>
                          <Button
                             title = "Call!"
                             color = "lightgrey"
                          />
                          <br/>
                        </Text>
                     </View>
                  ))
               }
            </ScrollView>
         </View>
      )
   }
}
export default ScrollViewExample

const styles = StyleSheet.create ({
  view:{
    padding: '3%',
    width:' 100%',
    margin: 'auto',
    boxShadow: '0px 1px 5px lightgrey'
  },
  Image:{
    width: '400px',
    height: '100px',
    margin: 'auto',
  },
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      flex: 1,
      backgroundColor: '#fff',
      padding:'2%',
      width: '98%',
      paddingLeft:'5%',
      borderRadius: '11px',
      boxShadow:' 0px 1px 6px lightgrey',
      margin: '2%',
      height:' fit-content'
   }
})